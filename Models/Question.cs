﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RazorWSV.Models
{
    public class Question
    {
        public int Id { get; set; }
        [DataType(DataType.Text)]
        public string Message { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime PublishedDate { get; set; }
    }
}